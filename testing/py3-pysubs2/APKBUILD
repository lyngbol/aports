# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-pysubs2
pkgver=1.7.2
pkgrel=0
pkgdesc="Python library for editing subtitle files"
url="https://github.com/tkarabela/pysubs2"
arch="noarch"
license="MIT"
makedepends="py3-setuptools py3-gpep517"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/tkarabela/pysubs2/archive/$pkgver/pysubs2-$pkgver.tar.gz"
builddir="$srcdir/pysubs2-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
53dc42911d473639449f96750ad1bade08035cf3588a5d6ab105db39415832f087666582f8f9678ca4a8ddb59f0f70fa54cf7538ef56b88a43049be1f8d006e3  pysubs2-1.7.2.tar.gz
"
