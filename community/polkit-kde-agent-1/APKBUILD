# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=polkit-kde-agent-1
pkgver=6.1.2
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="polkit-elogind"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	polkit-qt-dev
	qt6-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/polkit-kde-agent-1.git"
source="https://download.kde.org/stable/plasma/$pkgver/polkit-kde-agent-1-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
a0b7c6f9d248857ed7308b22c0f5485c2fda6615de500139d7f7c37a6ddf7829eefcb8516040cc1c6fecb725b139c96d71c23e2f8477d8bc48c9bef26554c81e  polkit-kde-agent-1-6.1.2.tar.xz
"
